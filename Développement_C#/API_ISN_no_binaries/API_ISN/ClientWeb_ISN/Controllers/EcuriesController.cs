﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using API_ISN.Models;
using ClientWPF_ISN.ControllersAPI;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;

namespace ClientWeb_ISN.Controllers
{
    public class EcuriesController : Controller
    {
        // GET: Ecuries
        public IActionResult Index()
        {
            IEnumerable<Ecurie> ecuries = API.Instance.GetEcuriesAsync().Result;
            return View(ecuries);
        }

        // GET: Ecuries/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return null;
            }
            return View(API.Instance.GetEcurieAsync(id).Result);
        }

        // GET: Ecuries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Ecuries/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Nom")] Ecurie ecurie)
        {
            if (ModelState.IsValid)
            {
                var URI = API.Instance.AjoutEcurieAsync(ecurie);
                return RedirectToAction(nameof(Index));
            }
            return View(ecurie);
        }
        // GET: Ecuries/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return null;
            }
            return View(API.Instance.GetEcurieAsync(id).Result);
        }

        // POST: Ecuries/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Nom")] Ecurie ecurie)
        {
            if (id != ecurie.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var URI = API.Instance.ModifEcurieAsync(ecurie);
                return RedirectToAction(nameof(Index));
            }
            return View(ecurie);
        }

        // GET: Ecuries/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return null;
            }
            return View(API.Instance.GetEcurieAsync(id).Result);
        }
        [HttpPost, ActionName("Delete")]
        // POST: Ecuries/Delete/5
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var URI = API.Instance.SupprEcurieAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
