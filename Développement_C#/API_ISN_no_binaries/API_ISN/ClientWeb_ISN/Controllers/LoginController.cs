﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ISN.Models;
using ClientWeb_ISN.ViewModels;
using ClientWPF_ISN.ControllersAPI;
using Microsoft.AspNetCore.Mvc;

namespace ClientWeb_ISN.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            UserViewModel viewModel = new UserViewModel { Authentifie = false };
            //if (HttpContext.User.Identity.IsAuthenticated)
            //{
            //    viewModel.User = API.Instance.GetUser(HttpContext.User.Identity.Name).Result;
            //}
            //else
            //{
            //    HttpContext.User.Identity.;
            //}
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(UserViewModel viewModel)
        {
            //if (ModelState.IsValid)
            {
                User user = API.Instance.GetUser(viewModel.User.Login, viewModel.User.Pwd).Result;
                if (user != null)
                {
                    return Redirect("/Ecuries/Index");
                }
                ModelState.AddModelError("User.Login", "Login et/ou mot de passe incorrect(s)");
            }
            return View(viewModel);
        }

        public ActionResult Deconnexion()
        {
            return Redirect("/");
        }
    }
}