﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using API_ISN.Models;
using Newtonsoft.Json;

namespace ClientWPF_ISN.ControllersAPI
{
    public sealed class API
    {
        private static readonly HttpClient client = new HttpClient();

        private API()
        {
            client.BaseAddress = new Uri("http://localhost:49382/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        private static readonly object padlock = new object();
        private static API instance = null;
        
        public static API Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new API();
                    }
                    return instance;
                }
            }
        }

        public async Task<User> GetUser(int id)
        {
            User user = null;
            HttpResponseMessage response = client.GetAsync("api/users/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                user = JsonConvert.DeserializeObject<User>(resp);
            }
            return user; 
        }

        public async Task<User> GetUser(string idString)
        {
            int id;
            if (int.TryParse(idString, out id))
                return await GetUser(id);
            return null;
        }

        public async Task<User> GetUser(string login, string pwd)
        {
            User user = null;
            HttpResponseMessage response = client.GetAsync("api/users/" + login + "/" + pwd).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                user = JsonConvert.DeserializeObject<User>(resp);
            }
            return user;
        }

        public async Task<ICollection<Ecurie>> GetEcuriesAsync()
        {
            ICollection<Ecurie> Ecuries = new List<Ecurie>();
            HttpResponseMessage response = client.GetAsync("api/ecuries").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                Ecuries = JsonConvert.DeserializeObject<List<Ecurie>>(resp);
            }
            return Ecuries;
        }

        public async Task<Ecurie> GetEcurieAsync(int? id)
        {
            Ecurie Ecurie = null;
            HttpResponseMessage response = client.GetAsync("api/ecuries/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                Ecurie = JsonConvert.DeserializeObject<Ecurie>(resp);
            }
            return Ecurie;
        }

        public async Task<Uri> AjoutEcurieAsync(Ecurie ecurie)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/ecuries", ecurie);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> ModifEcurieAsync(Ecurie ecurie)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/ecuries/" + ecurie.Id, ecurie);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> SupprEcurieAsync(int id)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/ecuries/" + id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        //*******************************************************************************************

    }
}
