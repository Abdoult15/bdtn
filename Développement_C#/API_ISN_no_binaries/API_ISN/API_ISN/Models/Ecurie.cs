﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ISN.Models
{
    public class Ecurie
    {
        public int Id { get; set; }
        public String Nom { get; set; }
        public ICollection<Pilote> Pilotes { get; set; }
        public ICollection<Monoplace> Monoplaces { get; set; }
        public Ecurie()
        {
            Pilotes = new List<Pilote>();
            Monoplaces = new List<Monoplace>();
        }
    }
}
