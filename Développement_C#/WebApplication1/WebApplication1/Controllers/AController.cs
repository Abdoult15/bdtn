﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AController : ControllerBase
    {
        private readonly WebApplication1Context _context;

        public AController(WebApplication1Context context)
        {
            _context = context;
        }

        // GET: api/A
        [HttpGet]
        public async Task<ActionResult<IEnumerable<A>>> GetA()
        {
            return await _context.A.ToListAsync();
        }

        // GET: api/A/5
        [HttpGet("{id}")]
        public async Task<ActionResult<A>> GetA(int id)
        {
            var a = await _context.A.FindAsync(id);

            if (a == null)
            {
                return NotFound();
            }

            return a;
        }

        // PUT: api/A/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutA(int id, A a)
        {
            if (id != a.id)
            {
                return BadRequest();
            }

            _context.Entry(a).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/A
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<A>> PostA(A a)
        {
            _context.A.Add(a);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetA", new { id = a.id }, a);
        }

        // DELETE: api/A/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<A>> DeleteA(int id)
        {
            var a = await _context.A.FindAsync(id);
            if (a == null)
            {
                return NotFound();
            }

            _context.A.Remove(a);
            await _context.SaveChangesAsync();

            return a;
        }

        private bool AExists(int id)
        {
            return _context.A.Any(e => e.id == id);
        }
    }
}
