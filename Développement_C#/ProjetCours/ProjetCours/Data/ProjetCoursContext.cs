﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetCours.Models;

namespace ProjetCours.Data
{
    public class ProjetCoursContext : DbContext
    {
        public ProjetCoursContext (DbContextOptions<ProjetCoursContext> options)
            : base(options)
        {
        }

        public DbSet<ProjetCours.Models.Lumiere> Lumiere { get; set; }

        public DbSet<ProjetCours.Models.ObjetConnecte> ObjetConnecte { get; set; }

        public DbSet<ProjetCours.Models.Piece> Piece { get; set; }

        public DbSet<ProjetCours.Models.Maison> Maison { get; set; }
    }
}
