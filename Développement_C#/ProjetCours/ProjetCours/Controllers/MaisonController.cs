﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetCours.Data;
using ProjetCours.Models;

namespace ProjetCours.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaisonController : ControllerBase
    {
        private readonly ProjetCoursContext _context;

        public MaisonController(ProjetCoursContext context)
        {
            _context = context;
        }

        // GET: api/Maison
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Maison>>> GetMaison()
        {
            return await _context.Maison.ToListAsync();
        }

        // GET: api/Maison/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Maison>> GetMaison(int id)
        {
            var maison = await _context.Maison.FindAsync(id);

            if (maison == null)
            {
                return NotFound();
            }

            return maison;
        }

        // PUT: api/Maison/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaison(int id, Maison maison)
        {
            if (id != maison.Id)
            {
                return BadRequest();
            }

            _context.Entry(maison).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaisonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Maison
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Maison>> PostMaison(Maison maison)
        {
            _context.Maison.Add(maison);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaison", new { id = maison.Id }, maison);
        }

        // DELETE: api/Maison/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Maison>> DeleteMaison(int id)
        {
            var maison = await _context.Maison.FindAsync(id);
            if (maison == null)
            {
                return NotFound();
            }

            _context.Maison.Remove(maison);
            await _context.SaveChangesAsync();

            return maison;
        }

        private bool MaisonExists(int id)
        {
            return _context.Maison.Any(e => e.Id == id);
        }
    }
}
