﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetCours.Data;
using ProjetCours.Models;

namespace ProjetCours.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ObjetConnecteController : ControllerBase
    {
        private readonly ProjetCoursContext _context;

        public ObjetConnecteController(ProjetCoursContext context)
        {
            _context = context;
        }

        // GET: api/ObjetConnecte
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ObjetConnecte>>> GetObjetConnecte()
        {
            return await _context.ObjetConnecte.ToListAsync();
        }

        // GET: api/ObjetConnecte/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ObjetConnecte>> GetObjetConnecte(int id)
        {
            var objetConnecte = await _context.ObjetConnecte.FindAsync(id);

            if (objetConnecte == null)
            {
                return NotFound();
            }

            return objetConnecte;
        }

        // PUT: api/ObjetConnecte/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutObjetConnecte(int id, ObjetConnecte objetConnecte)
        {
            if (id != objetConnecte.Id)
            {
                return BadRequest();
            }

            _context.Entry(objetConnecte).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetConnecteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ObjetConnecte
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ObjetConnecte>> PostObjetConnecte(ObjetConnecte objetConnecte)
        {
            _context.ObjetConnecte.Add(objetConnecte);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetObjetConnecte", new { id = objetConnecte.Id }, objetConnecte);
        }

        // DELETE: api/ObjetConnecte/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ObjetConnecte>> DeleteObjetConnecte(int id)
        {
            var objetConnecte = await _context.ObjetConnecte.FindAsync(id);
            if (objetConnecte == null)
            {
                return NotFound();
            }

            _context.ObjetConnecte.Remove(objetConnecte);
            await _context.SaveChangesAsync();

            return objetConnecte;
        }

        private bool ObjetConnecteExists(int id)
        {
            return _context.ObjetConnecte.Any(e => e.Id == id);
        }
    }
}
