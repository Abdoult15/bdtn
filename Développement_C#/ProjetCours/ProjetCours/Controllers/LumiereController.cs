﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetCours.Data;
using ProjetCours.Models;

namespace ProjetCours.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LumiereController : ControllerBase
    {
        private readonly ProjetCoursContext _context;

        public LumiereController(ProjetCoursContext context)
        {
            _context = context;
        }

        // GET: api/Lumiere
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Lumiere>>> GetLumiere()
        {
            return await _context.Lumiere.ToListAsync();
        }

        // GET: api/Lumiere/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Lumiere>> GetLumiere(int id)
        {
            var lumiere = await _context.Lumiere.FindAsync(id);

            if (lumiere == null)
            {
                return NotFound();
            }

            return lumiere;
        }

        // PUT: api/Lumiere/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLumiere(int id, Lumiere lumiere)
        {
            if (id != lumiere.Id)
            {
                return BadRequest();
            }

            _context.Entry(lumiere).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LumiereExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Lumiere
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Lumiere>> PostLumiere(Lumiere lumiere)
        {
            _context.Lumiere.Add(lumiere);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLumiere", new { id = lumiere.Id }, lumiere);
        }

        // DELETE: api/Lumiere/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Lumiere>> DeleteLumiere(int id)
        {
            var lumiere = await _context.Lumiere.FindAsync(id);
            if (lumiere == null)
            {
                return NotFound();
            }

            _context.Lumiere.Remove(lumiere);
            await _context.SaveChangesAsync();

            return lumiere;
        }

        private bool LumiereExists(int id)
        {
            return _context.Lumiere.Any(e => e.Id == id);
        }
    }
}
