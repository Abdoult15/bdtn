﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetCours.Models
{
    public class Maison
    {
        public int id;
        public string adresse;
        public string ville; 
        public double surface;
        public List<Piece> piece;

      /*  public Maison(int id, string adresse, string ville, double surface)
        {
            this.id = id;
            this.adresse= adresse;
            this.ville = ville;
            this.surface = surface;
            this.piece = new List<Piece>();
        }
      */
        public int Id { get; set; }
        public string Adresse
        {
            get
            {
                return this.adresse;
            }
            set 
            { 
                this.adresse = value; 
            }
        }
         
        public string Ville
        {
            get
            {
                return this.ville;
            }
            set
            {
                this.ville = value;
            }
        }

        public double Surface 
        { get 
            {
                return this.surface;
            }
            set 
            {
                this.surface = value;
            }
                }

        public List<Piece> Piece
        {
            get
            {
                return this.piece;
            }
            set
            {
                this.piece = value;
            }
        }
    }
}
