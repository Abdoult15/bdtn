﻿using API_ISN.Models;
using ClientWPF_ISN.ControllersAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWPF_ISN.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ICollection<Ecurie> ListeEcuries;
        public MainWindow()
        {
            InitializeComponent();

            ListeEcuries = API.Instance.GetEcuriesAsync().Result;
            foreach (Ecurie ecurie in ListeEcuries)
            {
                LbEcuries.Items.Add(ecurie.Nom);
            }
        }

        private void LbEcuries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Ecurie ecurie = ListeEcuries.ElementAt(LbEcuries.Items.IndexOf(LbEcuries.SelectedItem));
            LbPilotes.Items.Clear();
            foreach(Pilote p in ecurie.Pilotes)
            {
                LbPilotes.Items.Add(p.Nom + " - " + p.Age);
            }
            LbMonoplaces.Items.Clear();
            foreach(Monoplace m in ecurie.Monoplaces)
            {
                LbMonoplaces.Items.Add(m.Numero + " - " + m.Puissance);
            }
            
        }

        private void BtCreer_Click(object sender, RoutedEventArgs e)
        {
            if (TbEcurie.Text.Length > 0)
            {
                Ecurie ecurie = new Ecurie
                {
                    Nom = TbEcurie.Text
                };
                _ = API.Instance.AjoutEcurieAsync(ecurie);
                LbEcuries.Items.Add(TbEcurie.Text);
                ListeEcuries.Add(ecurie);
                MessageBox.Show("Ecurie ajoutée !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Le nom de l'écurie doit être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtLog_Click(object sender, RoutedEventArgs e)
        {
            if (TbLogin.Text.Length > 0)
            {
                if (PbPassword.Password.Length > 3)
                {
                    var user = API.Instance.GetUser(TbLogin.Text, PbPassword.Password).Result;
                    if (user == null)
                    {
                        MessageBox.Show("Login / mot de passe incorrects !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        MessageBox.Show("Bienvenue " + user.Name, "Connecté !", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Mot de passe trop court ! Minimum 4 caractères.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Le login doit être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
