﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_ISN.Data;
using API_ISN.Models;

namespace API_ISN.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcuriesController : ControllerBase
    {
        private readonly API_ISNContext _context;

        public EcuriesController(API_ISNContext context)
        {
            _context = context;
        }

        // GET: api/Ecuries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ecurie>>> GetEcurie()
        {
            return await _context.Ecurie
                                     .Include("Pilotes")
                                     .Include("Monoplaces")
                                     .ToListAsync();
        }

        // GET: api/Ecuries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ecurie>> GetEcurie(int id)
        {
            var ecurie = await _context.Ecurie.FindAsync(id);

            if (ecurie == null)
            {
                return NotFound();
            }

            return ecurie;
        }

        // PUT: api/Ecuries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEcurie(int id, Ecurie ecurie)
        {
            if (id != ecurie.Id)
            {
                return BadRequest();
            }

            _context.Entry(ecurie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EcurieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ecuries
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Ecurie>> PostEcurie(Ecurie ecurie)
        {
            _context.Ecurie.Add(ecurie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEcurie", new { id = ecurie.Id }, ecurie);
        }

        // DELETE: api/Ecuries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Ecurie>> DeleteEcurie(int id)
        {
            var ecurie = await _context.Ecurie.FindAsync(id);
            if (ecurie == null)
            {
                return NotFound();
            }

            _context.Ecurie.Remove(ecurie);
            await _context.SaveChangesAsync();

            return ecurie;
        }

        private bool EcurieExists(int id)
        {
            return _context.Ecurie.Any(e => e.Id == id);
        }
    }
}
