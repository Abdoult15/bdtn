﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_ISN.Data;
using API_ISN.Models;

namespace API_ISN.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonoplacesController : ControllerBase
    {
        private readonly API_ISNContext _context;

        public MonoplacesController(API_ISNContext context)
        {
            _context = context;
        }

        // GET: api/Monoplaces
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Monoplace>>> GetMonoplace()
        {
            return await _context.Monoplace.ToListAsync();
        }

        // GET: api/Monoplaces/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Monoplace>> GetMonoplace(int id)
        {
            var monoplace = await _context.Monoplace.FindAsync(id);

            if (monoplace == null)
            {
                return NotFound();
            }

            return monoplace;
        }

        // PUT: api/Monoplaces/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMonoplace(int id, Monoplace monoplace)
        {
            if (id != monoplace.Id)
            {
                return BadRequest();
            }

            _context.Entry(monoplace).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonoplaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Monoplaces
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Monoplace>> PostMonoplace(Monoplace monoplace)
        {
            _context.Monoplace.Add(monoplace);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMonoplace", new { id = monoplace.Id }, monoplace);
        }

        // DELETE: api/Monoplaces/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Monoplace>> DeleteMonoplace(int id)
        {
            var monoplace = await _context.Monoplace.FindAsync(id);
            if (monoplace == null)
            {
                return NotFound();
            }

            _context.Monoplace.Remove(monoplace);
            await _context.SaveChangesAsync();

            return monoplace;
        }

        private bool MonoplaceExists(int id)
        {
            return _context.Monoplace.Any(e => e.Id == id);
        }
    }
}
