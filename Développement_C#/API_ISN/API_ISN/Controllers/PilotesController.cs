﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_ISN.Data;
using API_ISN.Models;

namespace API_ISN.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PilotesController : ControllerBase
    {
        private readonly API_ISNContext _context;

        public PilotesController(API_ISNContext context)
        {
            _context = context;
        }

        // GET: api/Pilotes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pilote>>> GetPilote()
        {
            return await _context.Pilote.ToListAsync();
        }

        // GET: api/Pilotes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Pilote>> GetPilote(int id)
        {
            var pilote = await _context.Pilote.FindAsync(id);

            if (pilote == null)
            {
                return NotFound();
            }

            return pilote;
        }

        // PUT: api/Pilotes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPilote(int id, Pilote pilote)
        {
            if (id != pilote.Id)
            {
                return BadRequest();
            }

            _context.Entry(pilote).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PiloteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pilotes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Pilote>> PostPilote(Pilote pilote)
        {
            _context.Pilote.Add(pilote);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPilote", new { id = pilote.Id }, pilote);
        }

        // DELETE: api/Pilotes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pilote>> DeletePilote(int id)
        {
            var pilote = await _context.Pilote.FindAsync(id);
            if (pilote == null)
            {
                return NotFound();
            }

            _context.Pilote.Remove(pilote);
            await _context.SaveChangesAsync();

            return pilote;
        }

        private bool PiloteExists(int id)
        {
            return _context.Pilote.Any(e => e.Id == id);
        }
    }
}
