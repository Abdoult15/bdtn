﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API_ISN.Models;

namespace API_ISN.Data
{
    public class API_ISNContext : DbContext
    {
        public API_ISNContext (DbContextOptions<API_ISNContext> options)
            : base(options)
        {
        }

        public DbSet<API_ISN.Models.Pilote> Pilote { get; set; }

        public DbSet<API_ISN.Models.Monoplace> Monoplace { get; set; }

        public DbSet<API_ISN.Models.Ecurie> Ecurie { get; set; }

        public DbSet<API_ISN.Models.User> User { get; set; }
    }
}
