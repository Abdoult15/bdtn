﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ISN.Models
{
    [Table("Pilot")]
    public class Pilote
    {
        [Key]
        public int Id { get; set; }
        [Required][Column("Name")]
        public String Nom { get; set; }
        public int Age { get; set; }
        [ForeignKey("FK_Ecurie_Pilote")]
        public int EcurieId { get; set; }
    }
}
