﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ISN.Models
{
    public class ObjetConnecte
    {
        public int id;
        public string nom;
        public string adReseau;
        /*
        public ObjetConnecte(int id, string nom, string adReseau)
        {
            this.id = id;
            this.nom = nom;
            this.adReseau = adReseau;
        }
        */
        public int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }

        public string Nom
        {
            get
            {
                return this.nom;
            }
            set
            {
                this.nom = value;
            }
        }

        public string AdReseau
        {
            get
            {
                return this.adReseau;
            }
            set
            {
                this.adReseau = value;
            }
        
    }
}
}
