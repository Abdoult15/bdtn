﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace API_ISN.Models
{
    
    public class Piece
    {
        public int id;
        public string nom;
        public double surface;
        public List<ObjetConnecte> objet;
        /*
        public Piece(int id, string nom, double surface)
        {
            this.id = id;
            this.nom = nom;
            this.surface = surface;
            this.objet = new List<ObjetConnecte>();
        }
        */
        public int Id { get; set; }
        public string Nom { 
            get 
            { 
                return this.nom; 
            }
            set 
            { 
                this.nom = value; 
            }
        }

        public double Surface { 
            get { 
                return this.surface; 
            } 
            set { 
                this.surface = value; 
            } 
        }

        public List<ObjetConnecte> Objet
        {
            get
            {
                return this.objet;
            }
            set
            {
                this.objet = value;
            }
        }
        
    }
}
