﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ISN.Models
{
    public class Lumiere : ObjetConnecte
    {
        public string etat;
        public string couleur;
        public string reference;
        /*
        public Lumiere(int id, string nom, string adReseau,string etat,string couleur,string reference) : base(id,nom,adReseau)
        {
            this.etat = etat;
            this.couleur = couleur;
            this.reference = reference;
        }
        */
        public string Etat { get; set; }

        public string Couleur { get; set; }

        public string Reference { get; set; }
    }
}
