﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ISN.Models
{
    public class Monoplace
    {
        public int Id { get; set; }
        [Required][Column("Power")]
        public int Puissance { get; set; }
        [Required][Column("Number")]
        public int Numero { get; set; }
        [ForeignKey("FK_Ecurie_Monoplace")]
        public int EcurieId { get; set; }
    }
}
